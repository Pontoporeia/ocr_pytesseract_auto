# Inside the White Cube: The Ideology of the Gallery Space

Brian O’Doherty
Introduction by Thomas McEvilley

The Lapis Press

Santa Monica San Francisco
Copyright ©1976, 1986 by Brian O'Doherty
Printed in the United States of America
All rights reserved

This book may not be reproduced, in whole or in part, in any form (beyond that copying permitted by Sections 107 and 108 of the United States Copyright Law and except by reviewers for the public press), without written permission from the publishers.

The essays in this book originally appeared in Art forum magazine in 1976 in a somewhat different form.

First book edition 1986

90 89 88 87 86 54321]

The Lapis Press
1850 Union Street, Suite 466
San Francisco, CA 94123

ISBN 0-932499-14-7 cloth
ISBN 0-932499-05-8 paper
Library of Congress Catalog Card No. 85-081090
ie

Contents

Acknowledgements

Introduction by Thomas McEvilley

I. Notes on the Gallery Space

A Fable of Horizontal and Vertical .. .Modernism . . .The Properties of the Ideal Gallery ...The Salon. ..The Easel Picture .. .The Frame as Editor ...Photography . . .Impressionism . . .The Myth of the Picture Plane ...Matisse...Hanging .. .The Picture Plane as Simile .. .The Wall as Battleground and “Art”. .. The Installation Shot....

II. The Eye and the Spectator

Another Fable. . . Five Blank Canvasses .. . Paint, Picture Plane, Objects... Cubism and Collage ...Space...The Spectator... The Eye... Schwitters’s Merzbau . . . Schwitters’s Performances .. . Happenings and Environments ...Kienholz, Segal, Kaprow...Hanson,de Andrea... Eye, Spectator, and Minimalism .. . Paradoxes of Experience... Conceptual and Body Art....

II. Context as Content

The Knock at the Door... Duchamp’s Knock .. . Ceilings . . . 1,200 Bags of Coal. ..Gestures and Projects .. .The Mile of String .. .Duchamp’s “Body” ... Hostility to the Audience... The Artist and the Audience... The Exclusive Space ...The Seventies . . .The White Wall . . .The White Cube. ..Modernist Man . . .The Utopian Artist ....Mondrian’s Room...Mondrian, Duchamp, Lissitzky... .

Afterword






*For Sidney Yates,*
*who fights for the artist*





## Acknowledgements

My thanks to John Coplans, who published the original essays in Artforum when he
was editor, and to Charles Cowles, who was then the publisher. Ingrid Sischy, the present
editor, offered the writer every courtesy in collecting these essays for republication.

Jan Butterfield of The Lapis Press has been an ideal editor, and 1 am grateful to her for
her determination to put these articles in book form. Barbara Novak read the text closely,
as is her wont. Nancy Foote most kindly assembtied the photographs. Susan Lively typed
the text with precision and brio.

1 am indebted to Jack Stauffacher for his careful design of the book.

tam also grateful to the museums and galleries which permitted the publication of the
photographs which illustrate the text.

{ want specially to thank Thomas McEvilley for so cogently setting the context in his
introduction and Ann McCoy for suggesting the idea of the book.

I should also thank Maurice Tuchman for his invitation to lecture at the Los Angeles
County Museum of Art in January 1975, when the lecture “Inside the White Cube,

1855 -1974” was first delivered.



## Introduction

It has been the special genius of our century to investigate things
in relation to their context, to come to see the context as formative
on the thing, and, finally, to see the context as a thing itself. In this
classic essay, first published as a series of three articles in Artforum
in 1976, Brian O’Doherty discusses this turn toward context in
twentieth century art. He investigates, perhaps for the first time,
what the highly controlled context of the modernist gallery does
to the art object, what it does to the viewing subject, and, ina cru-
cial moment for modernism, how the context devours the object,
becoming it.

In the first of the three sections, O’Doherty describes the modern
gallery space as “constructed along laws as rigorous as those for
building a medieval church.” The basic principle behind these
laws, he notes, is that “The outside world must not come in, so
windows are usually sealed off. Walls are painted white. The ceil-
ing becomes the source of light....The art is free, as the saying
used to go, ‘to take on its own life.’” The purpose of such a setting
is not unlike the purpose of religious buildings —the artworks, like
religious verities, are to appear “untouched by time and its vicis-
situdes.” The condition of appearing out of time, or beyond time,
implies a claim that the work already belongs to posterity — that is,
it is an assurance of good investment. But it does strange things to
the presentness of life, which, after all, unfolds itself in time. “Art
exists in a kind of eternity of display, and though there is lots of
‘period’ (late modern) there is no time. This eternity gives the gal-
lery a limbolike status; one has to have died already to be there.”

In searching for the significance of this mode of exhibition one
must look to other classes of chambers that have been constructed
on similar principles. The roots of this chamber of eternal display
are to be found not in the history of art so much as the history of
religion, where they are in fact even more ancient than the medi-
eval church. Egyptian tomb chambers, for example, provide an
astonishingly close parallel. They too were designed to eliminate
awareness of the outside world. They too were chambers where an
illusion of eternal presence was to be protected from the flow of
time. They too held paintings and sculptures that were regarded as
magically contiguous with eternity and thus able to provide access
to it or contact with it. Before the Egyptian tomb, functionally
comparable spaces were the Paleolithic painted caves of the Mag-
dalenian and Aurignacian ages in France and Spain. There, too,
paintings and sculptures were found in a setting deliberately set
off from the outside world and difficult of access—most of the
famous cave galleries are nowhere near the entrances, and some
of them require exacting climbing and spelunking to get to them.

Such ritual spaces are symbolic reestablishments of the ancient
umbilicus which, in myths worldwide, once connected heaven
and earth. The connection is renewed symbolically for the pur-
poses of the tribe or, more specifically, of that caste or party in the
tribe whose special interests are ritually represented. Since this is a
space where access to higher metaphysical realms is made to seem
available, it must be sheltered from the appearance of change and
time. This specially segregated space is a kind of non-space, ultra-
space, or ideal space where the surrounding matrix of space-time
is symbolically annulled. In Paleolithic times the ultra-space filled
with painting and sculpture seems to have served the ends of magi-
cal restitution to the biomass; afterlife beliefs and rituals may have
been involved also. By Egyptian times these purposes had coa-
lesced around the person of the Pharaoh: assurance of his afterlife
through eternity was assurance of the sustenance of the state for
which he stood. Behind these two purposes may be glimpsed the
political interests of a class or ruling group attempting to consoli-
date its grip on power by seeking ratification from eternity. At one

level the process is a kind of sympathetic magic, an attempt to
obtain something by ritually presenting something else that is in
some way like the thing that is desired. If something like what one
wants is present, the underlying reasoning implies, then what one
wants may not be far behind. The construction of a supposedly
unchanging space, then, or a space where the effects of change are
deliberately disguised and hidden, is sympathetic magic to pro-
mote unchangingness in the real or non-ritual world; it is an
attempt to cast an appearance of eternality over the status quo in
terms of social values and also, in our modern instance, artistic
values.

The eternity suggested in our exhibition spaces is ostensibly
that of artistic posterity, of undying beauty, of the masterpiece. But
in fact it is a specific sensibility, with specific limitations and condi-
tionings, that is so glorified. By suggesting eternal ratification of a
certain sensibility, the white cube suggests the eternal ratification
of the claims of the caste or group sharing that sensibility. As a
ritual place of meeting for members of that caste or group, it cen-
sors out the world of social variation, promoting a sense of the
sole reality of its own point of view and, consequently, its endur-
ance or eternal rightness. Seen thus, the endurance of a certain
power structure is the end for which the sympathetic magic of the
white cube is devised.

In the second of the three sections of his essay, O'Doherty deals
with the assumptions about human selfhood that are involved in
the institutionalization of the white cube. “Presence before a work
of art,” he writes, ‘““means that we absent ourselves in favor of the
Eye and the Spectator.” By the Eye he means the disembodied
faculty that relates exclusively to formal visual means. The Spec-
tator is the attenuated and bleached-out life of the self from which
the Eye goes forth and which, in the meantime, does nothing else.
The Eye and the Spectator are all that is left of someone who has
“died,” as O'Doherty puts it, by entering into the white cube. In
return for the glimpse of ersatz eternity that the white cube affords
us—and as a token of our solidarity with the special interests of a
group— we give up our humanness and become the cardboard


Spectator with the disembodied Eye. For the sake of the intensity
of the separate and autonomous activity of the Eye we accept a
reduced level of life and self. In classical modernist galleries, as in
churches, one does not speak in a normal voice; one does not
laugh, eat, drink, lie down, or sleep; one does not get ill, go mad,
sing, dance, or make love. Indeed, since the white cube promotes
the myth that we are there essentially as spiritual beings—the Eye
is the Eye of the Soul—we are to be understood as tireless and above
the vicissitudes of chance and change. This slender and reduced
form of life is the type of behavior traditionally required in reli-
gious sanctuaries, where what is important is the repression of
individual interests in favor of the interests of the group.The essen-
tially religious nature of the white cube is most forcefully ex-
pressed by what it does to the humanness of anyone who enters it
and cooperates with its premises. On the Athenian Acropolis in
Plato’s day one did not eat, drink, speak, laugh, and so on.
O'Doherty brilliantly traces the development of the white cube
out of the tradition of Western easel painting. He then redirects
attention to the same developments from another point of view,
that of the anti-formalist tradition represented here by Duchamp’s
installations 1,200 Coal Bags (1938) and Mile of String (1942),
which stepped once and for all outside the frame of the painting
and made the gallery space itself the primary material to be altered
by art. When O’Doherty recommends these works by Duchamp to
the attention of artists of the seventies he implies that not a great
deal has been achieved in the last forty or fifty years in breaking
down the barriers of disinterest or disdain that separate the two
traditions. Such lack of communication is impressive, since artists
themselves have attempted to carry on this dialogue for a genera-
tion. Yves Klein, for example, exhibited an empty gallery called
“The Void” (Le vide) (1958); shortly thereafter Arman responded
with an exhibition called “The Full” (Le plein) (1960) in which he
dialecticized Klein’s positing of a transcendental space that is in
the world but not of it by filling the same gallery from floor to ceil-
ing and wall to wall with garbage. Michael Asher, James Lee Byars,
and others have used the empty exhibition space itself as their


primary material in various works -—not to mention the tradition
known as Light and Space. O’Doherty discovered the Way to ver-
balize these developments for the first time. His essay is an exam-
ple of criticism attempting to digest and analyze the recent past
and the present —or shall I say the recent present. He argues that
the communal mind of our culture went through a significant
shift that expressed itself in the prominence of the white cube as a
central material and expressive mode for art, as well as a fashion-
able style of displaying it. He identifies the transition in question
as modernism bringing “to an endpoint its relentless habit of self-
definition.” The defining of self means the purposeful neglect of
all that is other than self. It is a process increasingly reductive that
finally leaves the slate wiped clean.

The white cube was a transitional device that attempted to
bleach out the past and at the same time control the future by
appealing to supposedly transcendental modes of presence and
power. But the problem with transcendental principles is that by
definition they speak of another world, not this one. It is this other
world, or access to it, that the white cube represents. It is like
Plato’s vision of a higher metaphysical realm where form, shin-
ingly attenuated and abstract like mathematics, is utterly discon-
nected from the life of human experience here below. (Pure form
would exist, Plato felt, even if this world did not.) It is little recog-
nized how much this aspect of Platonism has to do with modernist
ways of thinking, and especially as a hidden controlling structure
behind modernist esthetics. Revived in part as a compensatory
reaction to the decline of religion, and promoted, however mistak-
enly, by our culture’s attention to the unchanging abstraction of
mathematics, the idea of pure form dominated the esthetics (and
ethics) from which the white cube emerged. The Pythagoreans of
Plato’s day, including Plato himself, held that the beginning was a
blank where there appeared inexplicably a spot which stretched
into a line, which flowed into a plane, which folded into a solid,
which cast a shadow, which is what we see. This set of elements —
point, line, surface, solid, simulacrum—conceived as contentless
except in their own-nature, is the primary equipment of much


modern art. The white cube represents the blank ultimate face of
light from which, in the Platonic myth, these elements unspeak-
ably evolve. In such types of thought, primary shapes and geomet-
ric abstractions are regarded as alive —in fact, as more intensely
alive than anything with a specific content. The white cube’s ulti-
mate meaning is this life-erasing transcendental ambition dis-
guised and converted to specific social purposes. O’Doherty’s
essays in this book are defenses of the real life of the world against
the sterilized operating room of the white cube—defenses of time
and change against the myth of the eternality and transcendence
of pure form. In fact, they embody this defense as much as they
express it. They are a kind of spooky reminder of time, illustrating
how quickly the newest realizations of today become the classical
insights of yesterday. Though it is common to say that modernism,
with its exacerbated rate of change or development, is over, that
rate of change not only remains but is increasing. Articles written
today will, by 1990, either have been forgotten or like these, will
have become classic.

Thomas McEvilley
New York City 1986


## I. Notes on the Gallery Space

A recurrent scene in sci-fi movies shows the earth withdrawing
from the spacecraft until it becomes a horizon, a beachball, a
grapefruit, a golf ball, a star. With the changes in scale, responses
slide from the particular to the general. The individual is replaced
by the race and we are a pushover for the race—a mortal biped, or
a tangle of them spread out below like a rug. From a certain height
people are generally good. Vertical distance encourages this
generosity. Horizontality doesn’t seem to have the same moral
virtue. Faraway figures may be approaching and we anticipate the
insecurities of encounter. Life is horizontal, just one thing after
another, a conveyer belt shuffling us toward the horizon. But his-
tory, the view from the departing spacecraft, is different. As the
scale changes, layers of time are superimposed and through them
We project perspectives with which to recover and correct the past.
No wonder art gets bollixed up in this process; its history, per-
ceived through time, is confounded by the picture in front of your
eyes, a witness ready to change testimony at the slightest percep-
tual provocation. History and the eye have a profound wrangle at
the center of this “constant” we call tradition.

All of us are now sure that the glut of history, rumor, and evi-
dence we call the modernist tradition is being circumscribed bya
horizon. Looking down, we see more clearly its “laws” of progress,
its armature hammered out of idealist philosophy, its military
metaphors of advance and conquest. What a sight it is—or was!
Deployed ideologies, trancendent rockets, romantic slums where
degradation and idealism obsessively couple, all those troops run-


ning back and forth in conventional wars. The campaign reports
that end up pressed between boards on coffee tables give us little
idea of the actual heroics. Those paradoxical achievements huddle
down there, awaiting the revisions that will add the avant-garde
era to tradition, or, as we sometimes fear, end it. Indeed, tradition
itself, as the spacecraft withdraws, looks like another piece of bric-
a-brac on the coffee table—no more than a kinetic assemblage
glued together with reproductions, powered by little mythic motors
and sporting tiny models of museums. And in its midst, one notices
an evenly lighted “cell” that appears crucial to making the thing
work: the gallery space.

The history of modernism is intimately framed by that space; or
rather the history of modern art can be correlated with changes in
that space and in the way we see it. We have now reached a point
where we see not the art but the space first. (A cliché of the age is to
ejaculate over the space on entering a gallery.) An image comes to
mind of a white, ideal space that, more than any single picture,
may be the archetypal image of twentieth century art; it clarifies
itself through a process of historical inevitability usually attached
to the art it contains.

The ideal gallery subtracts from the artwork all cues that inter-
fere with the fact that it is “art.” The work is isolated from every-
thing that would detract from its own evaluation of itself. This
gives the space a presence possessed by other spaces where con-
ventions are preserved through the repetition of a closed system of
values. Some of the sanctity of the church, the formality of the
courtroom, the mystique of the experimental laboratory joins with
chic design to produce a unique chamber of esthetics. So powerful
are the perceptual fields of force within this chamber that, once
outside it, art can lapse into secular status. Conversely, things
become art in a space where powerful ideas about art focus on
them. Indeed, the object frequently becomes the medium through
which these ideas are manifested and proffered for discussion—a
popular form of late modernist academicism (“ideas are more inter-
esting than art”). The sacramental nature of the space becomes
clear, and so does one of the great projective laws of modernism:


As modernism gets older, context becomes content. In a peculiar
reversal, the object introduced into the gallery “frames” the gallery
and its laws.

A gallery is constructed along laws as rigorous as those for build-
ing a medieval church. The outside world must not come in, so
windows are usually sealed off. Walls are painted white. The ceil-
ing becomes the source of light. The wooden floor is polished so
that you click along clinically, or carpeted so that you pad sound-
lessly, resting the feet while the eyes have at the wall. The art is
free, as the saying used to go, “to take on its own life.” The discreet
desk may be the only piece of furniture. In this context a standing
ashtray becomes almost a sacred object, just as the firehose ina
modern museum looks not like a firehose but an esthetic conun-
drum. Modernism’s transposition of perception from life to formal
values is complete. This, of course, is one of modernism’ fatal
diseases.

Unshadowed, white, clean, artificial—the space is devoted to the
technology of esthetics. Works of art are mounted, hung, scattered
for study. Their ungrubby surfaces are untouched by time and its
vicissitudes. Art exists in a kind of eternity of display, and though
there is lots of “period” (late modern), there is no time. This eter-
nity gives the gallery a limbolike status; one has to have died
already to be there. Indeed the presence of that odd piece of furni-
ture, your own body, seems superfluous, an intrusion. The space
offers the thought that while eyes and minds are welcome, space-
occupying bodies are not—or are tolerated only as kinesthetic
mannekins for further study. This Cartesian paradox is reinforced
by one of the icons of our visual culture: the installation shot, sans
figures. Here at last the spectator, oneself, is eliminated. You are
there without being there—one of the major services provided for
art by its old antagonist, photography. The installation shot is a
metaphor for the gallery space. In it an ideal is fulfilled as strongly
as in a Salon painting of the 1830s.

Indeed, the Salon itself implicitly defines what a gallery is, a
definition appropriate for the esthetics of the period. A gallery is a
place with a wall, which is covered with a wall of pictures. The

wall itself has no instrinsic esthetic; it is simply a necessity for an
upright animal. Samuel E B. Morse’s Exhibition Gallery at the
Louvre (1833) is upsetting to the modern eye: masterpieces as
wallpaper, each one not yet separated out and isolated in space
like a throne. Disregarding the (to us) horrid concatenation of
periods and styles, the demands made on the spectator by the
hanging pass our understanding. Are you to hire stilts to rise to the
ceiling or get on hands and knees to sniff anything below the
dado? Both high and low are underprivileged areas. You overhear
a lot of complaints from artists about being “skied” but nothing
about being “floored.” Near the floor, pictures were at least acces-
sible and could accommodate the connoisseur’s “near” look before
he withdrew to a more judicious distance. One can see the nine-
teenth century audience strolling, peering up, sticking their faces
in pictures and falling into interrogative groups a proper distance
away, pointing with a cane, perambulating again, clocking off the
exhibition picture by picture. Larger paintings rise to the top
(easier to see from a distance) and are sometimes tilted out from
the wall to maintain the viewer's plane; the “best” pictures stay in
the middle zone; small pictures drop to the bottom. The perfect
hanging job is an ingenious mosaic of frames without a patch of
wasted wall showing.

What perceptual law could justify (to our eyes) such a barbar-
ity? One and one only: Each picture was seen as a self-contained
entity, totally isolated from its slum-close neighbor by a heavy
frame around and a complete perspective system within. Space
was discontinuous and categorizable, just as the houses in which
these pictures hung had different rooms for different functions.
The nineteenth century mind was taxonomic, and the nineteenth
century eye recognized hierarchies of genre and the authority of
the frame.

How did the easel picture become such a neatly wrapped parcel
of space? The discovery of perspective coincides with the rise of
the easel picture, and the easel picture, in turn, confirms the prom-
ise of illusionism inherent in painting. There is a peculiar relation-
ship between a mural—painted directly on the wall—and a picture


 

Samuel EB. Morse, Exhibition Gallery at the Louvre, 1832 - 33,

courtesy Terra Museum of American Art, Evanston, Illinois
?

that hangs on a wall; a painted wall is replaced by a piece of port-
able wall. Limits are established and framed; miniaturization
becomes a powerful convention that assists rather than contradicts
illusion. The space in murals tends to be shallow; even when illu-
sion is an intrinsic part of the idea, the integrity of the wall is as
often reinforced, by struts of painted architecture, as denied. The
wall itself is always recognized as limiting depth (you don’t walk
through it), just as corners and ceiling (often in a variety of inven-
tive ways) limit size. Close up, murals tend to be frank about their
means —illusionism breaks down in a babble of method. You feel
you are looking at the underpainting and often can’t quite find
your “place.” Indeed, murals project ambiguous and wandering
vectors with which the spectator attempts to align himself. The
easel picture on the wall quickly indicates to him exactly where
he stands.

For the easel picture is like a portable window that, once set on
the wall, penetrates it with deep space. This theme is endlessly
repeated in northern art, where a window within the picture in
turn frames not only a further distance but confirms the window-
like limits of the frame. The magical, boxlike status of some smaller
easel pictures is due to the immense distances they contain and
the perfect details they sustain on close examination. The frame of
the easel picture is as much a psychological container for the artist
as the room in which the viewer stands is for him or her. The per-
spective positions everything within the picture along a cone of
space, against which the frame acts like a grid, echoing those cuts
of foreground, middleground, and distance within. One “steps”
firmly into such a picture or glides effortlessly, depending on its
tonality and color. The greater the illusion, the greater the invita-
tion to the spectator’s eye; the eye is abstracted from an anchored
body and projected as a miniature proxy into the picture to inhabit
and test the articulations of its space.

For this process, the stability of the frame is as necessary as an
oxygen tank is to a diver. Its limiting security completely defines
the experience within. The border as absolute limit is confirmed in
easel art up to the nineteenth century. Where it curtails or elides


subject matter, it does so in a way that strengthens the edge.The
classic package of perspective enclosed by the Beaux-Arts frame
makes it possible for pictures to hang like sardines. There is no
suggestion that the space within the picture is continuous with
the space on either side of it.

This suggestion is made only sporadically through the eight-
eenth and nineteenth centuries as atmosphere and color eat away
at the perspective. Landscape is the progenitor of a translucent
mist that puts perspective and tone/color in opposition, because
implicit in each are opposite interpretations of the wall they hang
on. Pictures begin to appear that put pressure on the frame. The
archetypal composition here is the edge-to-edge horizon, separat-
ing zones of sky and sea, often underlined by beach, with maybe
a figure facing, as everyone does, the sea. Formal composition is
gone; the frames within the frame (coulisses, repoussoirs, the Braille
of perspective depth) have slid away. What is left is an ambiguous
surface partly framed from the inside by the horizon. Such pictures
(by Courbet, Caspar David Friedrich, Whistler, and hosts of little
masters) are poised between infinite depth and flatness and tend
to read as pattern. The powerful convention of the horizon zips
easily enough through the limits of the frame.

These and certain pictures focusing on an indeterminate patch
of landscape that often looks like the “wrong” subject introduce
the idea of noticing something, of an eye scanning. This temporal
quickening makes the frame an equivocal, and not an absolute,
zone. Once you know that a patch of landscape represents a deci-
sion to exclude everything around it, you are faintly aware of the
space outside the picture. The frame becomes a parenthesis. The
separation of paintings along a wall, through a kind of magnetic
repulsion, becomes inevitable. And it was accentuated and largely
initiated by the new science-—or art— devoted to the excision of a
subject from its context: photography.

In a photograph, the location of the edge is a primary decision,
since it composes — or decomposes — what it surrounds. Eventually
framing, editing, cropping — establishing limits become major
acts of composition. But not so much in the beginning. There was


the usual holdover of pictorial conventions to do some of the work
of framing — internal buttresses made up of convenient trees and
knolls. The best early photographs reinterpret the edge without
the assistance of pictorial conventions. They /ower the tension on
the edge by allowing the subject matter to compose itself, rather
than consciously aligning it with the edge. Perhaps this is typical
of the nineteenth century. The nineteenth century looked at a sub-
ject—not at its edges. Various fields were studied within their
declared limits. Studying not the field but its limits, and defining
these limits for the purpose of extending them, is a twentieth cen-
tury habit. We have the illusion that we add to a field by extending
it laterally, not by going, as the nineteenth century might say in
proper perspective style, deeper into it. Even scholarship in both
centuries has a recognizably different sense of edge and depth, of
limits and definition. Photography quickly learned to move away
from heavy frames and to mount a print on a sheet of board. A
frame was allowed to surround the board after a neutral interval.
Early photography recognized the edge but removed its rhetoric,
softened its absolutism, and turned it into a zone rather than the
strut it later became. One way or another, the edge as a firm con-
vention locking in the subject had become fragile.

Much of this applied to impressionism, in which a major theme
was the edge as umpire of what’s in and what’s out. But this was
combined with a far more important force, the beginning of the
decisive thrust that eventually altered the idea of the picture, the
Way it was hung, and ultimately the gallery space: the myth of
flatness, which became the powerful logician in painting’s argu-
ment for self-definition. The development of a shallow literal space
(containing invented forms, as distinct from the old illusory space
containing “real” forms) put further pressures on the edge. The
great inventor here is, of course, Monet.

Indeed, the magnitude of the revolution he initiated is such that
there is some doubt his achievement matches it; for he is an artist
of decided limitations (or one who decided on his limitations and
stayed within them). Monet's landscapes often seem to have been
noticed on his way to or from the real subject. There is an impres-


 

Claude Monet, Water Lilies, 1920, triptych: each panel 6'6" x 14’,
courtesy Museum of Modern Art, New York,
Mrs. Simon Guggenheim Fund


sion that he is settling for a provisional solution; the very feature-
lessness relaxes your eye to look elsewhere. The informal subject
matter of Impressionism is always pointed out, but not that the
subject is seen through a casual glance, one not too interested in
what it’s looking at. What is interesting in Monet is “looking at”
this look—the integument of light, the often preposterous for-
mularization of a perception through a punctate code of color and
touch which remains (until near the end) impersonal.The edge
eclipsing the subject seems a somewhat haphazard decision that
could just as well have been made a few feet left or right. A signa-
ture of Impressionism is the way the casually chosen subject sof-
tens the edge’s structural role at a time when the edge is under
pressure from the increasing shallowness of the space. This dou-
bled and somewhat opposing stress on the edge is the prelude to
the definition of a painting as a self-sufficient object —a container
of illusory fact now become the primary fact itself—which sets us
on the high road to some stirring esthetic climaxes.

Flatness and objecthood usually find their first official text in
Maurice Denis’s famous statement in 1890 that before a picture is
subject matter, it is first of all a surface covered with lines and col-
ors. This is one of those literalisms that sounds brilliant or rather
dumb, depending on the Zeitgeist. Right now, having seen the end-
point to which nonmetaphor, nonstructure, nonillusion and non-
content can take you, the Zeitgeist makes it sound a little obtuse.
That picture plane—the ever-thinning integument of modernist
integrity —sometimes seems ready for Woody Allen and has indeed
attracted its share of ironists and wits. But this ignores that the
powerful myth of the picture plane received its impetus from the
centuries during which it sealed in unalterable systems of illusion.
Conceiving it differently in the modern era was an heroic adjust-
ment that signified a totally different world view, which was
trivialized into esthetics, into the technology of flatness.

The literalization of the picture plane is a great subject. As the
vessel of content becomes shallower and shallower, composition
and subject matter and metaphysics all overflow across the edge
until, as Gertrude Stein said about Picasso, the emptying out is

