# OCR_pytesseract_auto

##### requires pdf2image and pytesseract

### Current working script is the `converter.py` script

- creates a output directory in the script's working directory (or skips if already there)
- scans and finds the pdf inside the script's working directory
- converts pdf to image sequence in output file

### ToDo: 

- list all images in ouput directory
- use list to scan for text with pytesseract and append to texte file
- close file
- clean up images before pytesseract processing
- gui to imput file path
- recognize syntax and choose appropriate language ? or user input for language
- scan images// use image search engines ? 

### Howto:

You need pdf2image and pytesseract

`pip install pdf2image`

`pip install pytesseract`

Download the `converter.py` script.

You need the pdf you want converted in the same directory as the script.
Then just run the script :

`python3 converter.py`

 It will automatically detect the pdf and create ouput directories and stuff.
 
 I plan to make the script with arguments and eventually even a gui.
