
# import module
from pdf2image import convert_from_path
import os
try:
    from PIL import Image
except ImportError:
    import Image
import pytesseract

# the file extension we're looking for !
extensionforpdf = ['.pdf']

# using os.getcwd() to locate the script's working directory
filepath = os.getcwd()

print('\n' + '  The conversion will start shortly. Sit back and relax.')
print('\n' + '  The script path is --> ' + filepath + '\n \n *** \n')

# variable for image output dir name
img_directory = 'output'
image_output_path = os.path.join(filepath,img_directory)

print(' --> Preparing the output directory for the converted images \n')

try:
    os.mkdir(image_output_path)
    print(" Directory '% s' created" % img_directory + '\n')
except OSError as error:
    print( ' ' + img_directory + ' already exists ! skipping \n')


# to then search said working directory for a pdf
# used listdir instead of walk because it is assumed that the pdf is in main script directory and as such, we don't want other pdf files found.
# non-recursive search
for filename in os.listdir(filepath):
    # make sure not to search twice or else the script searches the string list that is filepath
    for e in extensionforpdf:
        if filename[-len(e):] == e:
            pdffile = os.path.join(filepath,filename)
			#countfile += 1
            #print(pdffile)


# Store Pdf with convert_from_path function

print(' \n *** \n\n --> Now converting pdf to image in the output directory : \n')

# checking that output directory is empty

check_for_content = os.listdir(image_output_path)

if len(check_for_content) == 0:
    # converting pdf to image in empty directory
    images = convert_from_path(pdffile)
    for i in range(len(images)):
	    # Save pages as images in the pdf
        images[i].save(image_output_path + '/page'+ str(i) +'.png', 'PNG')
        print(' page '+ str(i) + ' saved')
else:
    # the directory is not empty, so we assume the pdf was already converted to images per page
    print(' The output directory is not empty, pls make sure that it is.\n \n If you already converted the images, this will save you some time. \n')

print('\n Pdf convert to images per page successfully ! \n')


#print ('\n' + '     ' + path + '\n')

extensions = ['.png']

images_to_convert = []

print('\n --> Making list of images to be processed with pytesseract : \n')
for filename in os.listdir(image_output_path):
    for e in extensions:
        if filename[-len(e):] == e:
            # on ajoute le chemin complet à la liste des images valides
            images_to_convert.append(os.path.join(image_output_path,filename))

# sorting the list from low to high
images_to_convert.sort()

print('\n --> Processing with pytesseract and creating text_from_pdf.txt : \n')
#print(images_to_convert)

# variable for output text file
output_text_file = "text_from_pdf.txt"

# if there is already a file, then error and message, else makes the texte file
try:
    f = open(output_text_file, "x")
    print(" " + output_text_file + " has been created ! \n")
except FileExistsError:
    print (output_text_file + " already exists! skipping\n")

# takes list from images to convert and appends the pytesseract processing result to the text file
print(" --> Processing images and writing text to " + output_text_file)
for i in images_to_convert:
    f = open(output_text_file, "at")
    f.write(pytesseract.image_to_string(Image.open(i)))
    f.close()
print( " \n\n  *** Conversion complete! ***  \n\n The ouput can be found in the " + output_text_file + " in the directory of the main script.\n\n" )
